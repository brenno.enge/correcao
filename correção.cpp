/*
 Tarefa:

  Correção de 2 erros neste progama.

        1 - Ele mostra um registro a mais que foi alocado sem necessidade
        2 - Ele gera erro no momento de liberar a memória criada para arma
               zenar a lista encadeada.

    Pontuação:
        1 ponto por correção, totalizando 2 pontos.

    Data da entrega: 25/08

 Exemplo de lista encadeada com alocação dinâmica
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void);

struct Dados {
    unsigned int         id;
    char                    nome[60];
    struct Dados        *next;
} dados;

int
main
(void)
{
    struct Dados *pd, *ps, *old_pd;
    struct Dados d[100];
    int    c;

    pd = ps = &dados;

    printf ("Tamanho da estrutura: %lu\n", sizeof(dados));

    // Inicialização do ponteiro dentro da estrutura;

    dados.next = (struct Dados *)(NULL);

    for( c=0; c <= 10; c++)
    {
        pd->id = c;
        strcpy(pd->nome, "Nome do primeiro elemento");
        pd->next = (struct Dados *)(malloc(sizeof(struct Dados ) *1));
        if ( pd->next != (struct Dados *)(NULL) )
        {
            pd = pd->next;
            pd->next = (struct Dados *)(NULL);
        }
        else
        {
            printf("Erro de alocacao de memoria. saindo do loop ( %d ) \n",c);
            break;
        }
    }
    pd = ps;
    while ( pd != (struct Dados *)(NULL) )
    {
        printf("Id: %u\n", pd->id);
        printf("Nome: %s\n", pd->nome);
        printf("Next: %tX\n", pd->next);
        pd = pd->next;
    }
    // Removendo a memoria alocada pela instrução MALLOC
    pd = (struct Dados *)(NULL);
    while ( pd >= ps )
    {
        pd = ps;
        while ( pd != (struct Dados *)(NULL) )
        {
            old_pd=pd;
            pd != pd->next;
        }
        free(old_pd);
    }
    return 0;
}
